const { validationResult } = require('express-validator');
const { log } = require('./logger');


class BaseException {
  constructor(code = 500, error = 'BASE_EXCEPTION', message = null, response) {
    log.error(`${error}: ${message}`);
    response.status(code).json({ error, message });
  }
}


class ServerException extends BaseException {
  constructor(response, message) {
    super(500, 'SERVER_FAILED', message, response);
  }
}


class QueryException extends BaseException {
  constructor(response, message) {
    super(500, 'QUERY_FAILED', message, response);
  }
}


class NoValidUserException extends BaseException {
  constructor(response, message) {
    super(500, 'NO_VALID_USER', message, response);
  }
}


class InvalidTokenException extends BaseException {
  constructor(response, message) {
    super(500, 'INVALID_TOKEN', message, response);
  }
}


class NoValidItemsException extends BaseException {
  constructor(response, message) {
    super(500, 'NO_ITEMS_FOUND', message, response);
  }
}


class NoValidInvoiceException extends BaseException {
  constructor(response, message) {
    super(500, 'NO_RECEIPT_FOUND', message, response);
  }
}


class CheckoutFailedException extends BaseException {
  constructor(response, message) {
    super(500, 'CHECKOUT_FAILED', message, response);
  }
}


// generic sanitization error
const sanitizationFailed = (req, res, next) => {
  const errorFormatter = ({
    location, msg, param, value, nestedErrors,
  }) => ({ msg, param, value });
  const errors = validationResult(req).formatWith(errorFormatter);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  next();
};

module.exports = {
  ServerException,
  QueryException,
  NoValidUserException,
  InvalidTokenException,
  NoValidItemsException,
  NoValidInvoiceException,
  CheckoutFailedException,
  sanitizationFailed,
};
