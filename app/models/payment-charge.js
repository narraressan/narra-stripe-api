const { DataTypes, Model } = require('sequelize');
const { getUUID } = require('../utils/tokenize');
const db = require('./db-connect');
const Payment = require('./payment');


class Payment_Charge extends Model {}
Payment_Charge.init({
  _id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  _isCompleted: {
    type: DataTypes.BOOLEAN,
    defaultValue: () => false,
  },
  _isSuccessful: {
    type: DataTypes.BOOLEAN,
    defaultValue: () => null,
    allowNull: true,
  },
  invoiceId: {
    type: DataTypes.UUID,
    allowNull: false,
    references: {
      model: Payment,
      key: '_uuid',
    },
  },
  charge: {
    // JSON or JSONB are only supported in postgresql
    type: DataTypes.TEXT,
    get() { JSON.parse(this.getDataValue('charge')); },
    set(value) {
      this.setDataValue('charge', JSON.stringify(value));
    },
    allowNull: true,
  },
}, {
  sequelize: db,
  timestamps: true,
});


module.exports = Payment_Charge;
