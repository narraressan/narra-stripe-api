const dev = require('./dev');

module.exports = {
  ...dev,

  // extended settings
  env: process.env.ENVIRONMENT || 'PRODUCTION',
};
