const express = require('express');

const api = express.Router();
const {
  body, header, param, query,
} = require('express-validator');
const { isAuthentic } = require('../middlewares/is-authentic');
const {
  ServerException, QueryException, NoValidUserException, sanitizationFailed,
} = require('../utils/exceptions');
const { getEncryptedPassword, isValidPasswordHash } = require('../utils/tokenize');
const { PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH, AUTH_HEADER } = require('../utils/constants');
const UserController = require('../controllers/customer');


/**
 * [POST] `/user`
 * - creates new user
 * - takes an object of user credentials and returns with Ok
 *
 * @auth False
 * @query NA
 * @params NA
 * @body {
 *    {string} email,
 *    {string} fullname,
 *    {string} password
 * }
 * @success {
 *    {string} token
 * }
 * @error {
 *    {string} error,
 *    {string} message
 * }
 */
api.post('/user',
  [
    body('email').isEmail(),
    body('fullname').not().isEmpty({ ignore_whitespace: true }),
    body('password').not().isEmpty({ ignore_whitespace: true }),
    body('password').isLength({ min: PASSWORD_MIN_LENGTH, max: PASSWORD_MAX_LENGTH }).withMessage(`Password must be ${PASSWORD_MIN_LENGTH}-${PASSWORD_MAX_LENGTH} characters.`),
  ],
  sanitizationFailed,
  async (req, res) => {
    try {
      const { body } = req;
      const usrcont = new UserController();
      const newUser = await usrcont.createNewUser(body.email, body.fullname.trim(), body.password);

      res.json({ token: newUser.jwt });
    } catch (err) {
      new QueryException(res, err);
    }
  });


/**
 * [GET] `/user`
 * - view details of current logged user
 * - takes a userID and returns user details
 *
 * @auth True
 * @query NA
 * @params NA
 * @body NA
 * @success {
 *    {string} id,
 *    {string} token,
 *    {string} fullname,
 *    {string} email,
 *    {string} isDeleted
 * }
 * @error {
 *    {string} error,
 *    {string} message
 * }
 */
api.get('/user',
  [
    header(AUTH_HEADER).not().isEmpty({ ignore_whitespace: true }),
  ],
  sanitizationFailed,
  isAuthentic,
  async (req, res) => {
    try {
      const usrcont = await new UserController(req.get(AUTH_HEADER));
      const user = await usrcont.getRegisteredUser();

      (user != null) ? res.json(user) : new NoValidUserException(res, 'No valid user found.');
    } catch (err) {
      new QueryException(res, err);
    }
  });


module.exports = api;
