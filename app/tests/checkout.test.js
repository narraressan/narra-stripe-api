const supertest = require('supertest');
const _ = require('lodash')
const app = require('../server');
const { AUTH_HEADER } = require('../utils/constants');


let request = null
let server = null
let X_AUTH = null
let user = {
  email: 'johndoe@email.com',
  password: 'supersecret'
}
let availableItems = []
let invoiceId = null


// start and clean dependencies
beforeAll((done) => {
  server = app.listen(done);
  request = supertest.agent(server);
})
afterAll((done) => {
  server.close(done)
})


describe('Initial user actions', () => {
  it('should login registered user', async () => {
    const res = await request
      .post('/auth/login')
      .send(user)
    X_AUTH = res.body.token
    expect(res.statusCode).toEqual(200)
  })
})


describe('Check available items', () => {
  it('should get list of items', async () => {
    const res = await request
      .get('/items?page=1')
      .set(AUTH_HEADER, X_AUTH)
      .send()
    availableItems = res.body
    expect(res.statusCode).toEqual(200)
  })
})


describe('Execute a checkout process via stripe', () => {
  it('should create new payment transaction', async () => {
    let toBuy = []
    _.each(availableItems, (item) => {
      toBuy.push(item.id)
    })

    // Note: This is a bad test as it requires both mysql and rabbitMQ to be running
    const res = await request
      .post('/checkout/stripe')
      .set(AUTH_HEADER, X_AUTH)
      .send({
        source: 'test-source-stripe-token',
        items: toBuy
      })
    invoiceId = res.body.invoiceId
    expect(res.statusCode).toEqual(200)
  })
})


describe('Finish a payment process', () => {
  it('should complete the payment transaction', async () => {
    // this will execute the clearing of the payment
    const res = await request
      .put('/checkout/stripe')
      .set(AUTH_HEADER, X_AUTH)
      .send({
        invoiceId,
        charge_info: {
          stripe: 'this is a test charge'
        },
        successful: true
      })
    expect(res.statusCode).toEqual(200)
  })
})


describe('Check all invoices by current user', () => {
  it('should see an invoice for payment transaction', async () => {
    const res = await request
      .get(`/invoices?search=${invoiceId}&page=1`)
      .set(AUTH_HEADER, X_AUTH)
      .send()
    expect(res.body[0].invoice).toEqual(invoiceId)
    expect(res.statusCode).toEqual(200)
  })
})


describe('Check all invoices by current user', () => {
  it('should see the specific invoice details', async () => {
    const res = await request
      .get(`/invoice/${invoiceId}`)
      .set(AUTH_HEADER, X_AUTH)
      .send()
    expect(Object.keys(res.body)).toEqual(['isComplete','invoice','total','createdAt','items'])
    expect(res.statusCode).toEqual(200)
  })
})


describe('Logout user after all transactions', () => {
  it('should logout current user', async () => {
    const res = await request
      .post('/auth/logout')
      .set(AUTH_HEADER, X_AUTH)
      .send()
    expect(res.statusCode).toEqual(200)
  })
})