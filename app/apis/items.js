const express = require('express');

const api = express.Router();
const { body, header, param } = require('express-validator');
const { isAuthentic } = require('../middlewares/is-authentic');
const {
  ServerException, NoValidItemsException, QueryException, sanitizationFailed,
} = require('../utils/exceptions');
const { PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH, AUTH_HEADER } = require('../utils/constants');
const PurchaseController = require('../controllers/purchase');


/**
 * [GET] `/item/:itemId`
 * - view specific item
 * - takes an itemId and returns item details
 *
 * @auth True
 * @query NA
 * @params {
 *    {string} itemId
 * }
 * @body NA
 * @success {
 *    {string} id,
 *    {string} name,
 *    {string} description,
 *    {number} price,
 *    {string} currency,
 *    {string} thumbnail,
 *    {boolean} isDeleted,
 * }
 * @error {
 *    {string} error,
 *    {string} message
 * }
 */
api.get('/item/:itemId',
  [
    param('itemId').not().isEmpty({ ignore_whitespace: true }),
    header(AUTH_HEADER).not().isEmpty({ ignore_whitespace: true }),
  ],
  sanitizationFailed,
  isAuthentic,
  async (req, res) => {
    try {
      const pchscont = await new PurchaseController(req.get(AUTH_HEADER));
      const item = await pchscont.getItem(req.params.itemId);

      (item != null) ? res.json(item) : new NoValidItemsException(res, 'No valid item found.');
    } catch (err) {
      new QueryException(res, err);
    }
  });


/**
 * [GET] `/items`
 * - get list of items by page
 * - takes params as filters and returns list of items
 *
 * @auth True
 * @query {
 *    {number} page,        ** default: 1
 *    {string} search
 * }
 * @params NA
 * @body NA
 * @success [
 * {
 *    {string} id,
 *    {string} name,
 *    {string} description,
 *    {number} price,
 *    {date} date
 * },
 * ...]
 * @error {
 *    {string} error,
 *    {string} message
 * }
 */
api.get('/items',
  [
    header(AUTH_HEADER).not().isEmpty({ ignore_whitespace: true }),
  ],
  sanitizationFailed,
  isAuthentic,
  async (req, res) => {
    try {
      const pchscont = await new PurchaseController(req.get(AUTH_HEADER));
      const items = await pchscont.getAvailableItems(req.query.page, req.query.search);

      (items != null && items.length > 0) ? res.json(items) : new NoValidItemsException(res, 'No valid items found.');
    } catch (err) {
      new QueryException(res, err);
    }
  });


module.exports = api;
