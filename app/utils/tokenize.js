const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const uuidv4 = require('uuid/v4');
const { secretToken } = require('../config');


// generate a hash using the password
const getEncryptedPassword = async (password) => {
  const hash = await bcrypt.hash(password, 10);
  return hash;
};


// compare if 2 password are equal
const isValidPasswordHash = async (rawPassword, hashPassword) => {
  const isValid = await bcrypt.compare(rawPassword, hashPassword);
  return isValid;
};


// get safe uuid text
const getUUID = () => uuidv4();

// generate jwtToken
const getJWTToken = (user = {}) => jwt.sign(user, secretToken);


// check if jwt token is valie
const isValidJWTToken = (token) => {
  try {
    const decoded = jwt.verify(token, secretToken);
    return true;
  } catch (err) {
    return false;
  }
};


module.exports = {
  getEncryptedPassword, isValidPasswordHash, getUUID, getJWTToken, isValidJWTToken,
};
