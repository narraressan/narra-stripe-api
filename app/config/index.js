const dev = require('./dev');
const prod = require('./prod');


let config = dev;
if (process.env.ENVIRONMENT === 'PRODUCTION') {
  config = prod;
}


module.exports = config;
