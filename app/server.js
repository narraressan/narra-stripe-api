const express = require('express');
const corsPolicy = require('cors');
const bodyParser = require('body-parser');
const responseTime = require('response-time');
const helmet = require('helmet');
const compression = require('compression');


// internal dependencies
const { logRequestsToFile, logRequestsToConsole, log } = require('./utils/logger');
const authAPI = require('./apis/authenticate');
const userAPI = require('./apis/users');
const itemsAPI = require('./apis/items');
const paymentsAPI = require('./apis/payment');
const swaggerUI = require('./swaggerui/swagger');


// default config
const config = require('./config');


// instantiate the app
const app = express();


// setup default middlewares
app.use(corsPolicy());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(responseTime()); // sends the response time it took through X-Response-Time
app.use(logRequestsToFile());
app.use(logRequestsToConsole());
app.use(helmet());
app.use(compression());


// attach blueprints
app.use('/auth', authAPI);
app.use(userAPI);
app.use(itemsAPI);
app.use(paymentsAPI);
app.use(swaggerUI);


// start the app
app.listen(config.port, config.host, () => {
  log.info(`App@: ${config.host}:${config.port}`);
});


module.exports = app;
