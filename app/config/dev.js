const base = require('./base');

module.exports = {
  ...base,

  // extended settings
  env: process.env.ENVIRONMENT,
  QUEUE_HOST: process.env.QUEUE_HOST,
  QUEUE_PORT: process.env.QUEUE_PORT,
  QUEUE_USER: process.env.QUEUE_USER,
  QUEUE_PASS: process.env.QUEUE_PASS,
  DB_HOST: process.env.DB_HOST,
  DB_PORT: process.env.DB_PORT,
  DB_NAME: process.env.DB_NAME,
  DB_USER: process.env.DB_USER,
  DB_PASS: process.env.DB_PASS,
};
