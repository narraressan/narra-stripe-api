const { exec } = require('child_process');
const db = require('./models/db-connect');
const User = require('./models/user');
const Item = require('./models/item');
const Payment = require('./models/payment');
const Payment_item = require('./models/payment-item');
const Payment_charge = require('./models/payment-charge');
const { getEncryptedPassword } = require('./utils/tokenize');
const {
  DB_HOST,
  DB_PORT,
  DB_NAME,
  DB_USER,
  DB_PASS,
} = require('./config');


const sleep = (ms) => new Promise((resolve) => {
  setTimeout(resolve, ms);
});


const execCallback = (error, stdout, stderr) => {
  if (error) {
    console.log(`error: ${error.message}`);
    return;
  }
  if (stderr) {
    console.log(`stderr: ${stderr}`);
    return;
  }
  console.log(`stdout: ${stdout}`);
};


const createTables = () => {
  // create relationships
  User.hasMany(Payment, { foreignKey: 'customerId' });
  Payment.hasMany(Payment_item, { foreignKey: 'paymentId' });
  Item.hasMany(Payment_item, { foreignKey: 'itemId' });
  Payment.hasMany(Payment_charge, { foreignKey: 'invoiceId' });


  // create the tables using sequelize
  // Note: enableAlter should be false, if you need to alter, then use the migration script
  const enableAlter = false;
  User.sync({ alter: enableAlter });
  Item.sync({ alter: enableAlter });
  Payment.sync({ alter: enableAlter });
  Payment_item.sync({ alter: enableAlter });
  Payment_charge.sync({ alter: enableAlter });
};


const addDefaultContents = async () => {
  try {
    // add default items to sell
    const item1 = Item.build({
      alias: 'Turret - Marantz',
      description: 'The Marantz Pro Turret is the all-in-one solution for your broadcasting, streaming, and podcasting needs',
      price: 300.99,
      currency: 'USD',
      thumbnail: 'https://static.bhphoto.com/images/images2000x2000/1505145936_1340586.jpg',
    });
    const item2 = Item.build({
      alias: 'Apple MacBook Pro (13-inch, 8GB RAM, 128GB Storage) - Space Gray',
      description: '\
        Quad-core 8th-Generation Intel Core i5 Processor <br>\
        Brilliant Retina Display with True Tone technology <br>\
        Touch Bar and Touch ID <br>\
        Intel Iris Plus Graphics 645 <br>\
        Ultrafast SSD <br>\
        Two Thunderbolt 3 (USB-C) ports <br>\
        Up to 10 hours of battery life <br>\
        802.11AC Wi-Fi <br>\
        Latest Apple-designed keyboard <br>\
        Force touch trackpad',
      price: 1199.99,
      currency: 'USD',
      thumbnail: 'https://images-na.ssl-images-amazon.com/images/I/71IQiviMzWL._AC_SL1500_.jpg',
    });
    const item3 = Item.build({
      alias: 'iOttie Easy One Touch 4 Dash & Windshield Car Mount Phone Holder || for iPhone, Samsung, Moto, Huawei, Nokia, LG, Smartphones',
      description: '\
        EASY ONE TOUCH LOCK/RELEASE: Patented Easy One Touch mechanism allows quick one hand open and close operation <br>\
        ADJUSTABLE VIEWING: The newly re designed Telescopic Arm extends from 4 – 6. 5 inch and pivots on 225 degree arc for a variety of optimal positions <br>\
        UNIVERSAL MOUNTING: Holds all phone and case combinations from 2.3 inches 3.5 inches <br>\
        STRONG SUCTION: One time use dashboard disc and reusable suction cup combo offers superior stick strength <br>\
        BOTTOM FOOT: Foot on the bottom of Mount Cradle can be adjusted side to side, up and down, or removed to hold smartphones and cases of all sizes',
      price: 24.94,
      currency: 'USD',
      thumbnail: 'https://images-na.ssl-images-amazon.com/images/I/718NVofDrCL._AC_SL1500_.jpg',
    });


    // you may use .create() instead of .build.save()
    item1.save();
    item2.save();
    item3.save();


    // add default user - for testing
    const hashPass = await getEncryptedPassword('supersecret');
    const user = await User.create({
      fullname: 'John Doe',
      email: 'johndoe@email.com',
      password: hashPass,
    });
  } catch (err) {
    console.log('Adding default failed:', err.name);
  }


  // run post migrations with agnostic and scripts in ./migration
  // Note: here you may create the triggers, aggregates, and other special sql functions
  // create the Receipt view
  const agnostic = `agnostic -t mysql -u ${DB_USER} --password ${DB_PASS} -h ${DB_HOST} -p ${DB_PORT} -d ${DB_NAME} -m ./app/migrations`;
  exec(`${agnostic} bootstrap --no-load-existing`, (error) => console.log(`error: ${error}`));
  await sleep(5000);
  exec(`${agnostic} migrate --no-backup`, (error) => console.log(`error: ${error}`));
};


// connect to docker database
(async () => {
  let isDBAlive = false;
  let attempts = 1;

  // this is a very wrong and ugly implementation of reconnecting failing db
  while (isDBAlive !== true && attempts < 20) {
    try {
      const con = await db.authenticate();
      isDBAlive = true;
      console.log('DB connected.');

      createTables();
      await addDefaultContents();
    } catch (err) {
      console.error('Error:', err.name);
      if (err.name === 'SequelizeConnectionRefusedError') {
        console.log(`DB: ${attempts += 1} reconnection inprogress...\n\n`);
        await sleep(5000);
      } else {
        console.log('Execution failed:', err);
        break;
      }
    }
  }
})();
