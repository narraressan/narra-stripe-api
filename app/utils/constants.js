module.exports = {
  PASSWORD_MIN_LENGTH: 8,
  PASSWORD_MAX_LENGTH: 32,
  AUTH_HEADER: 'X-AUTH',
  PAGE_LIMIT: 10,
  QUEUE_NAME: 'narra-payments-queue',
};
