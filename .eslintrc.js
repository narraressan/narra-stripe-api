module.exports = {
  env: {
    browser: true,
    es6: true,
  },
  extends: [
    'airbnb-base',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  rules: {
    'no-unused-vars': 0,
    'max-classes-per-file': 0,
    'no-console': 0,
    'camelcase': 1,
    'consistent-return': 1,
    'no-shadow': 1,
    'no-multi-str': 1,
    'class-methods-use-this': 1,
    'no-new': 1,
    'no-underscore-dangle': 1,
    'no-unused-expressions': 0,
    'no-await-in-loop': 1,
    'no-throw-literal': 1
  },
};
