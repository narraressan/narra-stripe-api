const _ = require('lodash');


/**
 * an implementation of basic marshalling
 * NOTE: this functions marshals top level keys only
 *
 * @args {
 *    {json} source,
 *    {json} map --> refer to sequelize select statment `atttribute`
 * }
 * @return {
 *     {json} marshal
 * }
 */
const marshalling = (source, map) => {
  const marshallKeys = {};
  _.each(map, (key) => {
    if (Array.isArray(key)) {
      const [curName, newName] = key;
      marshallKeys[curName] = newName;
    } else {
      marshallKeys[key] = key;
    }
  });

  const tmp = _.pick(source, Object.keys(marshallKeys));
  const cleanMap = {};

  _.each(tmp, (value, key) => {
    cleanMap[marshallKeys[key]] = value;
  });

  return cleanMap;
};


module.exports = marshalling;
