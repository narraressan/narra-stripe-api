const { DataTypes, Model } = require('sequelize');
const { getUUID } = require('../utils/tokenize');
const db = require('./db-connect');
const Item = require('./item');
const Payment = require('./payment');


class Payment_Item extends Model {}
Payment_Item.init({
  _id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  paymentId: {
    type: DataTypes.UUID,
    allowNull: false,
    references: {
      model: Payment,
      key: '_uuid',
    },
  },
  itemId: {
    type: DataTypes.UUID,
    allowNull: false,
    references: {
      model: Item,
      key: '_uuid',
    },
  },
}, {
  sequelize: db,
  timestamps: true,
});


module.exports = Payment_Item;
