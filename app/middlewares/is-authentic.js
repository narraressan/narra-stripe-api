const { AUTH_HEADER } = require('../utils/constants');
const { ServerException, InvalidTokenException } = require('../utils/exceptions');
const { isValidJWTToken } = require('../utils/tokenize');


// determine if AUTH_HEADER is in a valid jwt format
const isAuthentic = (req, res, next) => {
  try {
    const authHeader = req.get(AUTH_HEADER);
    const isValid = isValidJWTToken(authHeader);
    isValid ? next() : new InvalidTokenException(res, 'Invalid authentication token.');
  } catch (err) {
    new ServerException(res, err);
  }
};


module.exports = { isAuthentic };
