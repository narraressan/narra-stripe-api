const supertest = require('supertest');
const app = require('../server');
const { AUTH_HEADER } = require('../utils/constants');


let request = null
let server = null
let X_AUTH = null
let user = null
let availableItems = []


// start and clean dependencies
beforeAll((done) => {
  server = app.listen(done);
  request = supertest.agent(server);
})
afterAll((done) => {
  server.close(done)
})


describe('Register new user', () => {
  it('should create new user', async () => {
    const randomName = Math.random().toString(36).substring(7);
    user = {
        email: `${randomName}@email.com`,
        fullname: `Test ${randomName}`,
        password: 'supertest',
      }

    const res = await request
      .post('/user')
      .send(user)
    expect(res.statusCode).toEqual(200)
  })
})


describe('Initial user actions', () => {
  it('should fail - password not in 8-32 chars', async () => {
    const res = await request
      .post('/auth/login')
      .send({
        ...user,
        password: '1234'
      })
    expect(res.statusCode).toEqual(422)
  })

  it('should login registered user', async () => {
    const res = await request
      .post('/auth/login')
      .send({
        email: user.email,
        password: user.password
      })
    X_AUTH = res.body.token
    expect(res.statusCode).toEqual(200)
  })

  it('should get details of current user', async () => {
    const res = await request
      .get('/user')
      .set(AUTH_HEADER, X_AUTH)
      .send()
    expect(res.body.token).toEqual(X_AUTH)
    expect(res.statusCode).toEqual(200)
  })
})


describe('Check available items', () => {
  it('should get list of items', async () => {
    const res = await request
      .get('/items')
      .set(AUTH_HEADER, X_AUTH)
      .send()
    availableItems = res.body
    expect(res.statusCode).toEqual(200)
  })
})


describe('Check specific item details', () => {
  it('should get details of a specific item', async () => {
    const res = await request
      .get(`/item/${availableItems[0].id}`)
      .set(AUTH_HEADER, X_AUTH)
      .send()
    expect(availableItems[0]).toEqual(res.body)
    expect(res.statusCode).toEqual(200)
  })
})


describe('Logout user after all transactions', () => {
  it('should logout current user', async () => {
    const res = await request
      .post('/auth/logout')
      .set(AUTH_HEADER, X_AUTH)
      .send()
    expect(res.statusCode).toEqual(200)
  })
})