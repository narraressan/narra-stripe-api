const { DataTypes, Model } = require('sequelize');
const { getUUID } = require('../utils/tokenize');
const db = require('./db-connect');


// this table assumes that the items can only be added via migration script
class Item extends Model {}
Item.init({
  _id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  _isDeleted: {
    type: DataTypes.BOOLEAN,
    defaultValue: () => false,
  },
  _uuid: {
    type: DataTypes.UUID,
    defaultValue: getUUID,
    unique: true,
  },
  alias: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true,
  },
  description: {
    type: DataTypes.TEXT,
    allowNull: false,
  },
  price: {
    type: DataTypes.DOUBLE,
    allowNull: false,
  },
  currency: {
    type: DataTypes.STRING,
  },
  thumbnail: {
    type: DataTypes.STRING,
  },
}, {
  sequelize: db,
  timestamps: true,
});


module.exports = Item;
