
CREATE OR REPLACE VIEW `Receipts` AS
SELECT
  p.`_isDeleted`,
  p.`_isComplete`,
  p.`_uuid` AS 'invoiceId',
  p.customerId,
  p.totalAmount,
  u.email,
  u.fullname,
  pi2.itemId,
  i.alias As 'item',
  i.description,
  i.currency,
  i.price,
  i.thumbnail,
  p.createdAt
FROM
  Payments p,
  Users u,
  Payment_Items pi2,
  Items i
WHERE
  p.customerId = u.`_uuid`
  AND p.`_uuid` = pi2.paymentId
  AND pi2.itemId = i.`_uuid`;