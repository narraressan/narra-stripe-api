const User = require('../models/user');
const { getEncryptedPassword, isValidPasswordHash, getJWTToken } = require('../utils/tokenize');
const marshall = require('../utils/marshall.js');


class UserController {
  // if User is not found, it will cause PurchaseController to be null instead of a new instance
  constructor(authToken = null) {
    this.currentUser = null;
    if (authToken != null) {
      return (
        async () => {
          this.currentUser = await User.findOne({ where: { jwt: authToken } });
          if (!this.currentUser) return null;
          return this;
        }
      )();
    }
  }


  /**
   * register new user in the database
   *
   * @args {
   *    {string} email,
   *    {string} fullname,
   *    {string} password
   * }
   * @return {
   *    {User} newUser
   * }
   */
  async createNewUser(email, fullname, rawPass) {
    const password = await getEncryptedPassword(rawPass);
    const newUser = await User.create({ email, fullname, password });
    await newUser.update(
      { jwt: getJWTToken() },
    );
    return newUser;
  }

  /**
   * get specific user from the database
   *
   * @args NA
   * @return {
   *    {User} registeredUser
   * }
   */
  async getRegisteredUser() {
    const registeredUser = await User.findOne({
      attributes: [
        ['_uuid', 'id'],
        ['jwt', 'token'],
        'fullname',
        'email',
        ['_isDeleted', 'isDeleted'],
      ],
      where: { jwt: this.currentUser.jwt },
    });
    return registeredUser;
  }

  /**
   * create new jwt token to a user
   *
   * @args {
   *    {string} email,
   *    {string} password
   * }
   * @return {
   *    {User} registeredUser
   * }
   */
  async loginRegisteredUser(email, password) {
    const registeredUser = await User.findOne({
      where: { email },
    });

    if (registeredUser) {
      const isValid = await isValidPasswordHash(password, registeredUser.password);

      if (isValid) {
        await registeredUser.update(
          { jwt: getJWTToken() },
          {
            where: {
              _uuid: registeredUser._uuid,
            },
            limit: 1,
          },
        );

        return marshall(registeredUser.dataValues, [
          ['_uuid', 'id'],
          ['jwt', 'token'],
          'fullname',
          'email',
          ['_isDeleted', 'isDeleted'],
        ]);
      }
      return null;
    }

    return null;
  }


  /**
   * delete existing jwt token from a user
   *
   * @args NA
   * @return {
   *    {User} registeredUser
   * }
   */
  async logoutRegisteredUser() {
    const registeredUser = await User.findOne({
      where: { jwt: this.currentUser.jwt },
    });

    if (registeredUser) {
      // if found, attach a new jwt token
      await registeredUser.update(
        { jwt: null },
      );

      return marshall(registeredUser.dataValues, [
        ['_uuid', 'id'],
        ['jwt', 'token'],
        'email',
      ]);
    }

    return null;
  }
}


module.exports = UserController;
