const { DataTypes, Model } = require('sequelize');
const { getUUID } = require('../utils/tokenize');
const db = require('./db-connect');


// this table models directly to a view - make sure not to run `.sync()``
class Receipt extends Model {}
Receipt.init({
  _isDeleted: {
    type: DataTypes.BOOLEAN,
  },
  _isComplete: {
    type: DataTypes.BOOLEAN,
  },
  invoiceId: {
    type: DataTypes.UUID,
  },
  customerId: {
    type: DataTypes.UUID,
  },
  totalAmount: {
    type: DataTypes.DOUBLE,
  },
  email: {
    type: DataTypes.STRING,
  },
  fullname: {
    type: DataTypes.STRING,
  },
  itemId: {
    type: DataTypes.UUID,
  },
  item: {
    type: DataTypes.STRING,
  },
  description: {
    type: DataTypes.STRING,
  },
  currency: {
    type: DataTypes.STRING,
  },
  price: {
    type: DataTypes.DOUBLE,
  },
  thumbnail: {
    type: DataTypes.STRING,
  },
  createdAt: {
    type: DataTypes.DATE,
  },
}, {
  sequelize: db,
});


module.exports = Receipt;
