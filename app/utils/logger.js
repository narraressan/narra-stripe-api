const { createLogger, format, transports } = require('winston');


const { combine, timestamp, printf } = format;
const morgan = require('morgan');
const file = require('rotating-file-stream');
const path = require('path');


// create a rotating write stream
const logsDir = '../logs';
const accessLogStream = file.createStream('requests.log', {
  size: '10M', // rotate every 10 MegaBytes written
  interval: '1d', // rotate daily
  path: path.join(__dirname, logsDir),
});


morgan.token('req-headers', (req, res) => JSON.stringify(req.headers));
morgan.token('req-body', (req, res) => JSON.stringify(req.body));
morgan.token('req-params', (req, res) => JSON.stringify(req.query));
const morganFormat = ':method :url :status :req-headers :req-body :req-params';


const logRequestsToFile = () => morgan(morganFormat, { stream: accessLogStream });


const logRequestsToConsole = () => morgan(morganFormat);


const log = createLogger({
  format: combine(
    timestamp(),
    format.printf(({ level, message, timestamp }) => `${timestamp} ${level}: ${message}`),
  ),
  transports: [
    new transports.Console(),
    new transports.File({ filename: path.join(__dirname, `${logsDir}/logs.log`) }),
  ],
});


module.exports = { logRequestsToFile, logRequestsToConsole, log };
