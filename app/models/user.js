const { DataTypes, Model } = require('sequelize');
const { getUUID } = require('../utils/tokenize');
const db = require('./db-connect');
const Payment = require('./payment');


// users can only buy an item not add an item
class User extends Model {}
User.init({
  _id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  _isDeleted: {
    type: DataTypes.BOOLEAN,
    defaultValue: () => false,
  },
  _uuid: {
    type: DataTypes.UUID,
    defaultValue: getUUID,
    unique: true,
  },
  jwt: {
    type: DataTypes.STRING,
    unique: true,
  },
  fullname: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  email: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true,
  },
  password: {
    type: DataTypes.STRING,
    allowNull: false,
  },
}, {
  sequelize: db,
  timestamps: true,
});


module.exports = User;
