const { Op, fn } = require('sequelize');
const amqp = require('amqplib');
const _ = require('lodash');
const Item = require('../models/item');
const User = require('../models/user');
const Payment = require('../models/payment');
const PaymentItem = require('../models/payment-item');
const PaymentCharge = require('../models/payment-charge');
const Receipt = require('../models/receipt');
const { PAGE_LIMIT, QUEUE_NAME } = require('../utils/constants');
const { log } = require('../utils/logger');
const {
  QUEUE_USER,
  QUEUE_PASS,
  QUEUE_HOST,
  QUEUE_PORT,
} = require('../config');


class PurchaseController {
  // if User is not found, it will cause PurchaseController to be null instead of a new instance
  constructor(authToken) {
    this.currentUser = null;
    this.queue = null;
    this.channel = null;

    return (
      async () => {
        this.currentUser = await User.findOne({ where: { jwt: authToken } });

        // setup rabbitmq channels
        this.queue = await amqp.connect(`amqp://${QUEUE_USER}:${QUEUE_PASS}@${QUEUE_HOST}:${QUEUE_PORT}`);
        this.channel = (this.queue != null) ? await this.queue.createChannel() : null;

        if (!this.currentUser) return null;
        return this;
      }
    )();
  }


  /**
   * get sellable items
   *
   * @args {
   *    {number} page,
   *    {text} search
   * }
   * @return {
   *    [Item] availableItems      ** array of items
   * }
   */
  async getAvailableItems(page = 1, search = null) {
    let searchFilter = {};

    if (search) {
      searchFilter = {
        [Op.or]: [
          { alias: { [Op.like]: `%${search}%` } },
          { description: { [Op.like]: `%${search}%` } },
        ],
      };
    }

    const availableItems = await Item.findAll({
      attributes: [
        ['_uuid', 'id'],
        ['alias', 'name'],
        'description',
        'price',
        'currency',
        'thumbnail',
        ['_isDeleted', 'isDeleted'],
      ],
      where: searchFilter,
      limit: PAGE_LIMIT,
      offset: ((page - 1) * PAGE_LIMIT),
    });

    return availableItems;
  }


  /**
   * get specific item
   *
   * @args {
   *    {string} itemId
   * }
   * @return {
   *    {Item} availableItem
   * }
   */
  async getItem(itemId) {
    const availableItems = await Item.findOne({
      attributes: [
        ['_uuid', 'id'],
        ['alias', 'name'],
        'description',
        'price',
        'currency',
        'thumbnail',
        ['_isDeleted', 'isDeleted'],
      ],
      where: {
        _uuid: itemId,
      },
    });

    return availableItems;
  }


  /**
   * get all receipts by current user
   *
   * @args {
   *    {number} page,
   *    {text} search
   * }
   * @return {
   *    [Receipt] purchaseReceipts      ** array of receipts
   * }
   */
  async getUserInvoices(page = 1, search = null) {
    let searchFilter = {
      customerId: this.currentUser._uuid,
    };

    if (search) {
      searchFilter = {
        [Op.and]: [
          { invoiceId: { [Op.like]: `%${search}%` } },
          { customerId: this.currentUser._uuid },
        ],
      };
    }

    const purchaseReceipts = await Receipt.findAll({
      attributes: [
        ['_isComplete', 'isComplete'],
        ['invoiceId', 'invoice'],
        ['totalAmount', 'total'],
        [fn('COUNT', 'invoiceId'), 'items'],
        'createdAt',
      ],
      where: searchFilter,
      group: ['invoiceId'],
      limit: PAGE_LIMIT,
      offset: ((page - 1) * PAGE_LIMIT),
    });

    return purchaseReceipts;
  }


  /**
   * get a specific receipt by current user
   *
   * @args {
   *    {string} invoiceNo
   * }
   * @return {
   *    {Receipt} purchaseReceipt
   * }
   */
  async getInvoice(invoiceId) {
    const purchaseReceipt = await Receipt.findAll({
      attributes: [
        ['_isComplete', 'isComplete'],
        ['invoiceId', 'invoice'],
        ['totalAmount', 'total'],
        'createdAt',
        'itemId',
        'item',
        'description',
        'currency',
        'price',
        'thumbnail',
        ['_isDeleted', 'isDeleted'],
      ],
      where: {
        customerId: this.currentUser._uuid,
        invoiceId,
      },
    });

    const summary = {
      isComplete: null,
      invoice: null,
      total: null,
      createdAt: null,
      items: [],
    };

    if (purchaseReceipt.length > 0) {
      const item = purchaseReceipt[0].dataValues;
      summary.isComplete = item.isComplete;
      summary.invoice = item.invoice;
      summary.total = item.total;
      summary.createdAt = item.createdAt;
    }

    _.each(purchaseReceipt, (item) => {
      summary.items.push({
        id: item.itemId,
        name: item.item,
        description: item.description,
        price: item.price,
        currency: item.currency,
        thumbnail: item.thumbnail,
        isDeleted: item.isDeleted,
      });
    });

    return summary;
  }


  /**
   * get a specific invoice transaction
   *
   * @args {
   *    {string} invoiceNo
   * }
   * @return {
   *    {Receipt} purchaseReceipt
   * }
   */
  async updateInvoice(invoiceId, status, chargeInfo) {
    try {
      const charge = await PaymentCharge.findOne({ where: { invoiceId } });
      await charge.update(
        {
          _isComplete: true,
          _isSuccessful: status,
          charge: chargeInfo,
        },
      );
      return charge;
    } catch (err) {
      log.error(err);
      return null;
    }
  }


  /**
   * make payment ---> send to payment service via queue
   *
   * @args {
   *    {string} source,       ** stripeToken generated from UI payment form
   *    [string] items         ** list of itemId of items to checkout
   * }
   * @return {
   *    {string} queueId
   * }
   */
  async checkoutWithStripe(currency, stripeToken, items) {
    try {
      let stripeTransaction = null;
      const paymentTransaction = {
        customerId: this.currentUser._uuid,
        totalAmount: 0,
      };

      // generate total amount from all `items`
      const checkoutItems = await Item.findAll({
        where: {
          _uuid: {
            [Op.in]: items,
          },
          currency,
        },
      });
      _.each(checkoutItems, (item) => {
        paymentTransaction.totalAmount += item.price;
      });

      // create a new entry in Payment table and use _uuid as description
      const payment = await Payment.create(paymentTransaction);
      await PaymentCharge.create({ invoiceId: payment.dataValues._uuid });

      // create new entry in PaymentItems for each items listed
      const cartItems = [];
      _.each(checkoutItems, (item) => {
        cartItems.push({
          paymentId: payment._uuid,
          itemId: item._uuid,
        });
      });
      if (cartItems.length === 0) throw {};

      await PaymentItem.bulkCreate(cartItems);

      stripeTransaction = {
        currency,
        amount: paymentTransaction.totalAmount,
        invoice: payment._uuid,
        token: stripeToken,
      };

      const stat = await this.channel.assertQueue(QUEUE_NAME);
      if (stat) {
        this.channel.sendToQueue(QUEUE_NAME, Buffer.from(JSON.stringify({
          ...stripeTransaction,

          // user must not logout / and jwt must not expire during the stripe charging
          auth: this.currentUser.jwt,
          user: this.currentUser._uuid,
        })));
      }

      return stripeTransaction;
    } catch (err) {
      log.info(err);
      return null;
    }
  }
}


module.exports = PurchaseController;
