const supertest = require('supertest');
const _ = require('lodash')
const app = require('../server');
const { AUTH_HEADER } = require('../utils/constants');


let request = null
let server = null
let X_AUTH = null
let user = {
  email: 'johndoe@email.com',
  password: 'supersecret'
}


// start and clean dependencies
beforeAll((done) => {
  server = app.listen(done);
  request = supertest.agent(server);
})
afterAll((done) => {
  server.close(done)
})


describe('Initial user actions', () => {
  it('should login registered user', async () => {
    const res = await request
      .post('/auth/login')
      .send(user)
    X_AUTH = res.body.token
    expect(res.statusCode).toEqual(200)
  })
})


describe('Check all invoices by current user', () => {
  it('should see all my invoices', async () => {
    const res = await request
      .get('/invoices')
      .set(AUTH_HEADER, X_AUTH)
      .send()
    let results = res.body

    if (results.error) {
      expect(res.statusCode).toEqual(500)
    }
    else {
      expect(res.statusCode).toEqual(200)
    }
  })
})


describe('Check all invoices by current user', () => {
  it('should see all my invoices in page 2', async () => {
    const res = await request
      .get('/invoices?page=2')
      .set(AUTH_HEADER, X_AUTH)
      .send()
    let results = res.body

    if (results.error) {
      expect(res.statusCode).toEqual(500)
    }
    else {
      expect(res.statusCode).toEqual(200)
    }
  })
})


describe('Logout user after all transactions', () => {
  it('should logout current user', async () => {
    const res = await request
      .post('/auth/logout')
      .set(AUTH_HEADER, X_AUTH)
      .send()
    expect(res.statusCode).toEqual(200)
  })
})