# narra-stripe-api

A generic and simple API to allow external request to post payment requests to the specific payment gateways.


### Setup local dev
``` bash
# install dependencies
npm install --verbose

# start the application
npm start:api
npm start:gateways

# to run tests, simply execute
# make sure docker is running?
npm run test

# setup database
pip install agnostic[mysql]
cd ./app
agnostic -t mysql -u [username] -d narra-payments bootstrap --no-load-existing
agnostic -t mysql -u [username] -d narra-payments migrate

# run and fix linter manually. 
# this should also be triggered during pre-commit
npm run lint
npm run lint:fix

# run dev via docker-compose - highly recommended!
# check localhost:15672 for the rabbitmq dashboard
docker-compose --file ./docker/dev/dev.yml up -d --build

# view logs
docker-compose --file ./docker/dev/dev.yml logs -f --tail 10
```


### Build and Publish docker
Note: this is not production ready yet


### Future releases
- connect with `narra-payment-service` image for processing payments


> more to come soon probably a full blown service derived from this simple project ...