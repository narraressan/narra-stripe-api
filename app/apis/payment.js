const express = require('express');

const api = express.Router();
const { body, param, header } = require('express-validator');
const { isAuthentic } = require('../middlewares/is-authentic');
const {
  ServerException,
  NoValidInvoiceException,
  QueryException,
  CheckoutFailedException,
  sanitizationFailed,
} = require('../utils/exceptions');
const { PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH, AUTH_HEADER } = require('../utils/constants');
const PurchaseController = require('../controllers/purchase');


/**
 * [POST] `/checkout/stripe`
 * - create new payment transaction
 * - takes an object with list of itemIds to pay and returns with queueId from the rabbitMQ
 *
 * @auth True
 * @query NA
 * @params NA
 * @body {
 *    {string} invoiceId
 * }
 * @success {
 *    {string} source,
 *    [string] items,       ** list of itemId
 * }
 * @error {
 *    {string} error,
 *    {string} message
 * }
 */
api.post('/checkout/stripe',
  [
    header(AUTH_HEADER).not().isEmpty({ ignore_whitespace: true }),

    // stripeToken generated from stripe
    body('source').not().isEmpty({ ignore_whitespace: true }),

    // list of itemId to be checkedout
    body('items').not().isEmpty({ ignore_whitespace: true }),
  ],
  sanitizationFailed,
  isAuthentic,
  async (req, res) => {
    try {
      const pchscont = await new PurchaseController(req.get(AUTH_HEADER));

      // default currency is USD, maybe in the future may support multiple currencies
      const checkout = await pchscont.checkoutWithStripe('USD', req.body.source, req.body.items);

      (checkout != null) ? res.json({ invoiceId: checkout.invoice }) : new CheckoutFailedException(res, 'Checkout failed.');
    } catch (err) {
      new QueryException(res, err);
    }
  });


/**
 * [PUT] `/checkout/stripe`
 * - update payment transaction status (stripe_charges)
 *
 * @auth True --- token must be from the user who sent it
 * @query NA
 * @params NA
 * @body {
 *    {string} invoiceId,
 *    {string} charge_info,
 *    {boolean} successful
 * }
 * @success Ok
 * @error {
 *    {string} error,
 *    {string} message
 * }
 */
api.put('/checkout/stripe',
  [
    header(AUTH_HEADER).not().isEmpty({ ignore_whitespace: true }),
    body('invoiceId').not().isEmpty({ ignore_whitespace: true }),
    body('successful').not().isEmpty({ ignore_whitespace: true }),

    // charge_info generated from stripe transaction - JSON string
    body('charge_info').not().isEmpty({ ignore_whitespace: true }),
  ],
  sanitizationFailed,
  isAuthentic,
  async (req, res) => {
    try {
      // this AUTH_HEADER may expire if the user has logged out - and the transaction may be lost
      const pchscont = await new PurchaseController(req.get(AUTH_HEADER));

      const charge = await pchscont.updateInvoice(
        req.body.invoiceId,
        req.body.successful,
        req.body.charge_info,
      );

      (charge != null) ? res.json('Ok') : new CheckoutFailedException(res, 'Checkout clearing stage failed.');
    } catch (err) {
      new QueryException(res, err);
    }
  });


/**
 * [GET] `/invoice/:invoiceNo`
 * - get specific invoice details
 * - takes a invoiceNo and returns payment details
 *
 * @auth True
 * @query NA
 * @params {
 *    {string} invoiceNo
 * }
 * @body NA
 * @success {
 *    {boolean} isComplete,
 *    {string} invoice,
 *    {number} total,
 *    {string} createdAt,
 *    [item] items
 * }
 * @error {
 *    {string} error,
 *    {string} message
 * }
 */
api.get('/invoice/:invoiceNo',
  [
    param('invoiceNo').not().isEmpty({ ignore_whitespace: true }),
    header(AUTH_HEADER).not().isEmpty({ ignore_whitespace: true }),
  ],
  sanitizationFailed,
  isAuthentic,
  async (req, res) => {
    try {
      const pchscont = await new PurchaseController(req.get(AUTH_HEADER));
      const invoice = await pchscont.getInvoice(req.params.invoiceNo);

      (invoice != null) ? res.json(invoice) : new NoValidInvoiceException(res, 'No valid invoice found.');
    } catch (err) {
      new QueryException(res, err);
    }
  });


/**
 * [GET] `/invoices`
 * - get list of invoices by page
 * - takes specific filter via args and returns list of current user's payment invoices
 *
 * @auth True
 * @query {
 *    {number} page,        ** default: 1
 *    {string} search
 * }
 * @params NA
 * @body NA
 * @success [
 *      {boolean} isComplete,
 *      {string} invoice,
 *      {number} total,
 *      {number} items,       **count of items
 * ...]
 * @error {
 *    {string} error,
 *    {string} message
 * }
 */
api.get('/invoices',
  [
    header(AUTH_HEADER).not().isEmpty({ ignore_whitespace: true }),
  ],
  sanitizationFailed,
  isAuthentic,
  async (req, res) => {
    try {
      const pchscont = await new PurchaseController(req.get(AUTH_HEADER));
      const invoices = await pchscont.getUserInvoices(req.query.page, req.query.search);

      (invoices != null && invoices.length > 0) ? res.json(invoices) : new NoValidInvoiceException(res, 'No valid invoices found.');
    } catch (err) {
      new QueryException(res, err);
    }
  });


module.exports = api;
