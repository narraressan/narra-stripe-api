const { DataTypes, Model } = require('sequelize');
const { getUUID } = require('../utils/tokenize');
const db = require('./db-connect');
const User = require('./user');


class Payment extends Model {}
Payment.init({
  _id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  _isDeleted: {
    type: DataTypes.BOOLEAN,
    defaultValue: () => false,
  },
  _uuid: {
    type: DataTypes.UUID,
    defaultValue: getUUID,
    unique: true,
  },
  _isComplete: {
    type: DataTypes.BOOLEAN,
    defaultValue: () => false,
  },
  customerId: {
    type: DataTypes.UUID,
    allowNull: false,
    references: {
      model: User,
      key: '_uuid',
    },
  },
  totalAmount: {
    type: DataTypes.DOUBLE,
    allowNull: false,
  },
}, {
  sequelize: db,
  timestamps: true,
});


module.exports = Payment;
