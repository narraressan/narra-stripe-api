const express = require('express');

const api = express.Router();
const { body, header } = require('express-validator');
const { isAuthentic } = require('../middlewares/is-authentic');
const {
  ServerException, NoValidUserException, QueryException, sanitizationFailed,
} = require('../utils/exceptions');
const { PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH, AUTH_HEADER } = require('../utils/constants');
const UserController = require('../controllers/customer');


/**
 * [POST] `/login`
 * - validate user login with jwt token
 * - takes an object with email and password then returns a JWT token
 *
 * @auth False
 * @query NA
 * @params NA
 * @body {
 *    {string} email,
 *    {string} password
 * }
 * @api-success {
 *    {string} id,
 *    {string} token,
 *    {string} fullname,
 *    {string} email,
 *    {boolean} isDeleted
 * }
 * @api-error {
 *    {string} error,
 *    {string} message
 * }
 */
api.post('/login',
  [
    body('email').isEmail(),
    body('password').not().isEmpty({ ignore_whitespace: true }),
    body('password').isLength({ min: PASSWORD_MIN_LENGTH, max: PASSWORD_MAX_LENGTH }).withMessage(`Password must be ${PASSWORD_MIN_LENGTH}-${PASSWORD_MAX_LENGTH} characters.`),
  ],
  sanitizationFailed,
  async (req, res) => {
    try {
      const { body } = req;
      const usrcont = await new UserController(req.get(AUTH_HEADER));
      const user = await usrcont.loginRegisteredUser(body.email, body.password);

      (user != null) ? res.json(user) : new NoValidUserException(res, 'No valid user found.');
    } catch (err) {
      new QueryException(res, err);
    }
  });


/**
 * [POST] `/logout`
 * - invalidates jwt token and logs out the user
 * - takes a header `AUTH_HEADER` with active token and returns Ok
 *
 * @auth True
 * @query NA
 * @params NA
 * @body NA
 * @api-success Ok
 * @api-error {
 *    {string} error,
 *    {string} message
 * }
 */
api.post('/logout',
  [
    header(AUTH_HEADER).not().isEmpty({ ignore_whitespace: true }),
  ],
  sanitizationFailed,
  isAuthentic,
  async (req, res) => {
    try {
      const usrcont = await new UserController(req.get(AUTH_HEADER));
      const user = await usrcont.logoutRegisteredUser();

      (user != null) ? res.json('Ok') : new NoValidUserException(res, 'No valid user found.');
    } catch (err) {
      new QueryException(res, err);
    }
  });


module.exports = api;
