const Sequelize = require('sequelize');
const {
  DB_HOST,
  DB_PORT,
  DB_NAME,
  DB_USER,
  DB_PASS,
} = require('../config');


const dbAddress = `mysql://${DB_USER}:${DB_PASS}@${DB_HOST}:${DB_PORT}/${DB_NAME}`;
const db = new Sequelize(dbAddress);


module.exports = db;
